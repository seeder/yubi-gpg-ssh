#!/bin/sh +xe 

# Author: Mateusz Pawlowski <mateusz@generik.co.uk>
# License: GPL-2

f=`tempfile`
echo "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"1050\", ATTRS{idProduct}==\"0116\", OWNER=\"${USERNAME}\"" > $f
sudo cp $f /etc/udev/rules.d/99-yubikeys.rules
rm $f
sudo aptitude install scdaemon gnupg-agent gnupg2

mkdir ~/.gnupg
cp gpg-agent.conf ~/.gnupg
echo RELOADAGENT | gpg-connect-agent
ssh-add -L >> ~/.ssh/authorized_keys
